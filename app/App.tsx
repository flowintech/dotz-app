import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Index } from './src/screens/login';
import { SignIn } from './src/screens/login/signIn';
import InnerHomeScreen from './src/screens/home';
import { SignUp } from './src/screens/login/signUp';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Index"
        screenOptions={{ headerShown: false }}
      >
        <Stack.Screen name="Index" component={Index} />
        <Stack.Screen name="SignIn" component={SignIn} />
        <Stack.Screen name="SignUp" component={SignUp} />
        <Stack.Screen name="Home" component={InnerHomeScreen} />


      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;

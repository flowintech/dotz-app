import { IUser } from "../interfaces/IUser";
import { setUser } from "../stores/user";

const endpoint: string = 'https://60afd848e6d11e00174f5407.mockapi.io/';

export const getUserByUsername = async (username: string) => {
  await fetch(
    'https://60afd848e6d11e00174f5407.mockapi.io/user?username=' + username,
    {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
    .then(response => response.json())
    .then(data => {
      setUser(data[0]);
    })
    .catch(err => console.error(err));
}

export const postUser = async (user: IUser) => {
  await fetch('https://60afd848e6d11e00174f5407.mockapi.io/user/',
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(user)
    }
  )
}
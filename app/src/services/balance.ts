import { IBalanceState, setBalance } from "../stores/balance";

export const getBalance = async (userId: string) => {
  await fetch(
    'https://60afd848e6d11e00174f5407.mockapi.io/user/' + { userId } + '/balance/' + { userId },
    {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
    .then(response => response.json())
    .then((data: IBalanceState) => {
      setBalance(data);
    })
    .catch(err => console.error(err));
}
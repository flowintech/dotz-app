import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { colors } from '../../constants/colors';

export const Index: React.FC = ({ navigation }: any) => {

  return (
    <View style={css.body}>

      <View style={css.titleWrapper}>
        <Text style={css.title}>Welcome to</Text>
        <Text style={css.title}>Dotz Digital Account!</Text>
      </View>
      
      <Text style={css.subtitle}>Your life yields more</Text>
      <Text style={css.subtitle}>here with us.</Text>

      <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'space-between' }}>

        <TouchableOpacity activeOpacity={0.7} onPress={() => navigation.navigate('SignUp')}>
          <View style={css.buttonSign}>
            <Text style={css.buttonSignText}>SIGN UP</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity activeOpacity={0.7} onPress={() => navigation.navigate('SignIn')}>
          <View style={css.buttonSign}>
            <Text style={css.buttonSignText}>SIGN IN</Text>
          </View>
        </TouchableOpacity>

      </View>

    </View>
  )
};

const css = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: colors.orange,
    padding: 24,
    paddingTop: 200
  },
  title: {
    fontSize: 30,
    fontWeight: "bold",
    color: colors.white
  },
  titleWrapper: {
    marginBottom: 16
  },
  subtitle: {
    color: colors.white,
    fontSize: 16
  },
  buttonSignText: {
    color: colors.white,
    fontWeight: '700'
  },
  buttonSign: {
    height: 36,
    minWidth: 64,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 6,
    paddingHorizontal: 16,
    borderRadius: 2
  },
});


// Your life yields more here with us.
import { plug } from 'luffie';
import React from 'react';
import { StyleSheet, Text, TextInput, ToastAndroid, TouchableOpacity, View } from 'react-native';
import TextInputMask from 'react-native-text-input-mask';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { map } from 'rxjs/operators';
import { colors } from '../../constants/colors';
import { IUser } from '../../interfaces/IUser';
import { IUserState, userStore, checkUsername, checkPassword, setUser, getUserFromStore } from '../../stores/user';

interface IProps {
  user?: IUser;
}

export const InnerSignUp: React.FC = ({ navigation }: any, props: IProps) => {

  const [textInputUsername, setTextInputUsername] = React.useState('')
  const [textInputPassword, setTextInputPassword] = React.useState('')
  const [textInputFirstName, setTextInputFirstName] = React.useState('')
  const [textInputLastName, setTextInputLastName] = React.useState('')
  const [textInputMail, setTextInputMail] = React.useState('')
  const [textInputPhone, setTextInputPhone] = React.useState('')

  const [isSignUpCompleted, setIsSignUpCompleted] = React.useState(false);
  const [usernameError, setUsernameError] = React.useState(false);

  const handleTextInputPassword = (password: string) => {
    setTextInputPassword(password)
    setIsSignUpCompleted(true)
    // if (password.length <= 6) {
    //   ToastAndroid.showWithGravity(
    //     "Your password must be 6 characters long or more.",
    //     ToastAndroid.SHORT,
    //     ToastAndroid.CENTER
    //   );
    // }
  }

  async function handleCheckButton() {

    const user = getUserFromStore();

    await setUser(undefined)
    const exists: boolean = await checkUsername(textInputUsername);

    switch (exists) {
      case true:
        ToastAndroid.showWithGravity(
          "This username has already been taken.",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
        break;

      case false:
        ToastAndroid.showWithGravity(
          "Great choice!",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
        setUsernameError(true)
        break;
    }
  }

  const handleSignInButton = () => {
    const passwordError = checkPassword(textInputPassword);
  }

  return (
    <>
      <View style={css.header}>

        <TouchableOpacity activeOpacity={0.7} onPress={() => navigation.navigate('Index')}>
          <View style={{ width: 40, height: 40 }}>
            <Icon name="chevron-left" size={42} color={colors.white} />
          </View>
        </TouchableOpacity>

        <TouchableOpacity activeOpacity={0.7} onPress={() => navigation.navigate('SignIn')}>
          <View style={
            {
              height: 36,
              minWidth: 64,
              backgroundColor: colors.white,
              borderRadius: 2,
              paddingHorizontal: 16,
              justifyContent: 'center'
            }
          }>
            <Text style={css.headerTitle}>SIGN IN</Text>
          </View>
        </TouchableOpacity>

      </View>

      <View style={{ flex: 1, paddingHorizontal: 16, paddingVertical: 24, backgroundColor: colors.orange }}>

        <View style={{ flex: 1 }}>

          <Text style={{ color: colors.white, fontWeight: '700', marginBottom: 12 }}>
            Fill the following fields for signing up
          </Text>

          <TextInput
            style={{ backgroundColor: colors.white, borderRadius: 2, paddingLeft: 8, marginBottom: 12, height: 36 }}
            placeholderTextColor={colors.gray}
            placeholder='Username'
            returnKeyLabel='next'
            selectTextOnFocus={true}
            value={textInputUsername}
            onChangeText={(value: string) => {
              setTextInputUsername(value)
              setUsernameError(false)
            }}
          />

          {textInputUsername === '' && (
            <View style={{ marginBottom: 12, backgroundColor: colors.white, opacity: 0.5, height: 36, width: '100%', borderRadius: 2, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ color: colors.orange, fontWeight: '700' }}>CHECK USERNAME</Text>
            </View>
          )}

          {textInputUsername !== '' && (
            <TouchableOpacity activeOpacity={0.7} onPress={() => handleCheckButton()}>
              <View style={{ marginBottom: 12, backgroundColor: colors.white, height: 36, width: '100%', borderRadius: 2, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ color: colors.orange, fontWeight: '700' }}>CHECK USERNAME</Text>
              </View>
            </TouchableOpacity>
          )}

          <TextInput
            style={{ width: '100%', backgroundColor: colors.white, borderRadius: 2, paddingLeft: 8, marginBottom: 12, height: 36 }}
            placeholderTextColor={colors.gray}
            placeholder='Password'
            secureTextEntry={true}
            editable={usernameError}
            returnKeyType='next'
            selectTextOnFocus={true}
            value={textInputPassword}
            onChangeText={(value: string) => {
              handleTextInputPassword(value)
            }}
          />

          <TextInput
            style={{ backgroundColor: colors.white, borderRadius: 2, paddingLeft: 8, marginBottom: 12, height: 36 }}
            placeholderTextColor={colors.gray}
            placeholder='First name'
            returnKeyLabel='next'
            selectTextOnFocus={true}
            value={textInputFirstName}
            onChangeText={(value: string) => {
              setTextInputFirstName(value)
              setUsernameError(false)
            }}
          />

          <TextInput
            style={{ backgroundColor: colors.white, borderRadius: 2, paddingLeft: 8, marginBottom: 12, height: 36 }}
            placeholderTextColor={colors.gray}
            placeholder='Last name'
            returnKeyLabel='next'
            selectTextOnFocus={true}
            value={textInputLastName}
            onChangeText={(value: string) => {
              setTextInputLastName(value)
              setUsernameError(false)
            }}
          />

          <TextInput
            style={{ backgroundColor: colors.white, borderRadius: 2, paddingLeft: 8, marginBottom: 12, height: 36 }}
            placeholderTextColor={colors.gray}
            placeholder='Mail'
            returnKeyLabel='next'
            selectTextOnFocus={true}
            value={textInputMail}
            onChangeText={(value: string) => {
              setTextInputMail(value)
              setUsernameError(false)
            }}
          />

          <TextInputMask
            style={{ backgroundColor: colors.white, borderRadius: 2, paddingLeft: 8, marginBottom: 12, height: 36 }}
            placeholderTextColor={colors.gray}
            placeholder='Phone'
            returnKeyLabel='next'
            selectTextOnFocus={true}
            value={textInputPhone}
            onChangeText={(value: string) => {
              setTextInputPhone(value)
              setUsernameError(false)
            }}
            mask={"+[00] ([00]) [00000] [0000]"}
          />

          {!isSignUpCompleted && (
            <View style={{ backgroundColor: colors.white, opacity: 0.5, height: 36, width: '100%', borderRadius: 2, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ color: colors.orange, fontWeight: '700' }}>PROCEED</Text>
            </View>
          )}

          {isSignUpCompleted && (
            <TouchableOpacity activeOpacity={0.7} onPress={() => handleProceedButton()}>
              <View style={{ backgroundColor: colors.white, height: 36, width: '100%', borderRadius: 2, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ color: colors.orange, fontWeight: '700' }}>PROCEED</Text>
              </View>
            </TouchableOpacity>
          )}

        </View>
      </View>
    </>
  )
};

const stream = (props: any) => {
  return userStore.pipe(map((userStore: IUserState) => ({
    user: userStore.user,
  })));
}

export const SignUp = plug(stream)(InnerSignUp);

const css = StyleSheet.create({
  header: {
    backgroundColor: colors.orange,
    height: 60,
    paddingTop: 12,
    paddingRight: 16,
    paddingLeft: 8,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  headerTitle: { fontSize: 16, color: colors.orange, fontWeight: '700' },
  textInput: { fontSize: 20, borderBottomWidth: 1, borderColor: colors.lightGray }

});

import { plug } from 'luffie';
import React from 'react';
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { map } from 'rxjs/operators';
import { colors } from '../../constants/colors';
import { IUser } from '../../interfaces/IUser';
import { IUserState, userStore, checkUsername, checkPassword } from '../../stores/user';

interface IProps {
  user?: IUser;
}

export const InnerSignIn: React.FC = ({ navigation }: any, props: IProps) => {

  const [textInputUsername, setTextInputUsername] = React.useState('')
  const [textInputPassword, setTextInputPassword] = React.useState('')
  const [usernameExists, setUsernameExists] = React.useState(false);
  const [usernameError, setUsernameError] = React.useState(false);
  const [passwordError, setPasswordError] = React.useState(false);

  async function handleProceedButton() {
    const exists = await checkUsername(textInputUsername)

    switch (exists) {
      case true:
        setUsernameExists(true);
        break;

      case false:
        setUsernameError(true);
        break;

      default:
        console.warn('erro')
        break;
    }
  }

  const handleSignInButton = () => {
    const passwordError = checkPassword(textInputPassword);
    passwordError ? navigation.navigate('Home') : setPasswordError(true);
  }

  return (
    <>
      <View style={css.header}>

        <TouchableOpacity activeOpacity={0.7} onPress={() => navigation.goBack()}>
          <View style={{ width: 40, height: 40 }}>
            <Icon name="chevron-left" size={42} color={colors.white} />
          </View>
        </TouchableOpacity>

        <TouchableOpacity activeOpacity={0.7} onPress={() => navigation.navigate('SignUp')}>
          <View style={
            {
              height: 36,
              minWidth: 64,
              backgroundColor: colors.white,
              borderRadius: 2,
              paddingHorizontal: 16,
              justifyContent: 'center'
            }
          }>
            <Text style={css.headerTitle}>SIGN UP</Text>
          </View>
        </TouchableOpacity>

      </View>

      <View style={{ flex: 1, paddingHorizontal: 16, paddingVertical: 24, backgroundColor: colors.orange }}>

        <View style={{ flex: 1 }}>

          <TextInput
            editable={!usernameExists}
            style={{ width: '100%', backgroundColor: colors.white, borderRadius: 2, paddingLeft: 8, marginBottom: 12, height: 36 }}
            placeholderTextColor={colors.gray}
            placeholder='Username'
            returnKeyLabel='next'
            selectTextOnFocus={true}
            value={textInputUsername}
            onChangeText={(value: string) => {
              setTextInputUsername(value)
              setUsernameError(false)
            }}
          />

          {usernameError && (
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Icon name='error' color={colors.white} size={18} />
              <Text style={{ color: colors.white, marginLeft: 6 }}>Invalid.</Text>
            </View>
          )}

          {usernameExists && (
            <TextInput
              style={{ width: '100%', backgroundColor: colors.white, borderRadius: 2, paddingLeft: 8, marginBottom: 12, height: 36 }}
              placeholderTextColor={colors.gray}
              placeholder='Password'
              secureTextEntry={true}
              returnKeyType='go'
              selectTextOnFocus={true}
              value={textInputPassword}
              onChangeText={(value: string) => {
                setTextInputPassword(value)
                setPasswordError(false)
              }}
            />
          )}

          {passwordError && (
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Icon name='error' color={colors.white} size={18} />
              <Text style={{ color: colors.white, marginLeft: 6 }}>Invalid.</Text>
            </View>
          )}



        </View>

        {!usernameExists && (
          <TouchableOpacity activeOpacity={0.7} onPress={() => handleProceedButton()}>
            <View style={{ backgroundColor: colors.white, height: 36, width: '100%', borderRadius: 2, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ color: colors.orange, fontWeight: '700' }}>PROCEED</Text>
            </View>
          </TouchableOpacity>
        )}

        {usernameExists && (
          <TouchableOpacity activeOpacity={0.7} onPress={() => handleSignInButton()}>
            <View style={{ backgroundColor: colors.white, height: 36, width: '100%', borderRadius: 2, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ color: colors.orange, fontWeight: '700' }}>SIGN IN</Text>
            </View>
          </TouchableOpacity>
        )}
      </View>

    </>)
};

const stream = (props: any) => {
  return userStore.pipe(map((userStore: IUserState) => ({
    user: userStore.user,
  })));
}

export const SignIn = plug(stream)(InnerSignIn);

const css = StyleSheet.create({
  header: {
    backgroundColor: colors.orange,
    height: 60,
    paddingTop: 12,
    paddingRight: 16,
    paddingLeft: 8,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  headerTitle: { fontSize: 16, color: colors.orange, fontWeight: '700' },
  textInput: { fontSize: 20, borderBottomWidth: 1, borderColor: colors.lightGray }

});

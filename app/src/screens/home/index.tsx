import React, { useState } from 'react';
import { ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { colors } from '../../constants/colors';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { setUser } from '../../stores/user';

const InnerHomeScreen = ({ navigation }) => {
  const [focusedBackdrop, setFocusedBackdrop] = useState(true);

  return (
    <>
      <View style={{ flex: 1, paddingHorizontal: 16, paddingVertical: 24, backgroundColor: colors.orange }}>
        <TouchableOpacity onPress={() => {
          setUser(undefined)
          navigation.navigate('Index')
        }}>
          <Text style={{}}>Sign out</Text>
        </TouchableOpacity>

      </View>

    </>
  )
};
const css = StyleSheet.create({
  backdropStyle: {
    backgroundColor: '#F4F4F5',
  },
  header: {
    backgroundColor: colors.orange,
    height: 60,
    paddingTop: 12,
    paddingRight: 16,
    paddingLeft: 8,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  headerTitle: { fontSize: 18, color: colors.white, fontWeight: '700' },
  textInput: { fontSize: 20, borderBottomWidth: 1, borderColor: colors.lightGray }

});

export default InnerHomeScreen;

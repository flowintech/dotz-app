export interface ISignUpRequest {
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  mail: string;
  phone: string;
}
export interface IUser {
  id: any;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  mail: string;
  phone: string;
}
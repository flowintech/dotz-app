import { createStore } from "luffie";
import { ISignInRequest } from "../interfaces/ILoginRequest";
import { ISignUpRequest } from "../interfaces/ISignUpRequest";
import { IUser } from "../interfaces/IUser";
import { getUserByUsername } from "../services/user";
import { IBalanceState } from "./balance";

export interface IUserState {
  user?: IUser;
  signUpRequest?: ISignUpRequest;
  signInRequest?: ISignInRequest;
}

const initialState: IUserState = {}

const { state$, updateState, getCurrentState } = createStore(initialState);

const checkPassword = (password: string): boolean | undefined => {
  const { user } = getCurrentState();
  if (user) {
    let userPassword = user.password;

    if (password == userPassword) {
      return true;
    } else {
      return false;
    }
  }
}

async function checkUsername(username: string) {
  setUser(undefined)
  await getUserByUsername(username);

  const user = getUserFromStore()
  if (user) {
    return true
  } else {
    return false
  }
}

const setUser = (user: IUser | undefined) => {
  updateState({ user })
}

const getUserFromStore = (): IUser | undefined => {
  const { user } = getCurrentState();

  return user
}

const setSignUpRequest = (
  username: string,
  password: string,
  firstName: string,
  lastName: string,
  mail: string,
  phone: string) => {

  const signUpRequest: ISignUpRequest = {
    username, password, firstName, lastName, mail, phone
  }

  updateState({ signUpRequest })

}

export {
  state$ as userStore,

  checkUsername,
  checkPassword,
  getUserFromStore,
  setUser,
}
import { createStore } from "luffie";

export interface IBalanceState {
  id?: any,
  userId?: string,
  dotz?: string,
  real?: string
}

const initialState: IBalanceState = {}

const { state$, updateState, getCurrentState } = createStore(initialState);

const setBalance = (balance: IBalanceState) => {
  updateState({ id: balance.id, userId: balance.userId, dotz: balance.dotz, real: balance.real })
}

export {
  state$ as balanceStore,

  setBalance,
}
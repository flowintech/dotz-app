# Dotz #

### Quem sou eu? ###

Eu sou o Mateus (sem H) Bernardo de Sousa (com S). Tenho 23 anos e estou procurando oportunidade para evoluir minha carreira profissional.

Falo inglês fluentemente e meu português é quase perfeito, rs.

Tenho pouco mais de 1 ano de experiência com React Native. Atualmente atuo num projeto como fullstack, front-end no React, back-end no .NET, mobile no React Native, banco de dados em PostgreSQL e DevOps com Jenkins, Bitbucket, Jira.

Mesmo atuando como fullstack, o meu foco é o mobile, já que o front-end e o back-end do projeto já estava bem sólido quando entrei para a equipe. 

Trabalhei na Tegra desenvolvendo o App do Assaí Atacadista. Depois participei no desenvolvimento de uma webAPI em PHP e front-end (CSS, HTML, JS).

Eu tenho uma filha de 1 ano que é a coisa mais linda do mundo e a minha maior motivação para conquistar meu espaço como desenvolvedor, e a minha maior felicidade é o trabalho remoto porque me permite participar do crescimento da minha filha.

Minhas metas para os próximos 5 anos:

- Construir uma casa;

- Terminar minha faculdade de Análise e Desenvolvimento de Sistemas e partir para Engenharia de Produção.

- Deixar meu primeiro carro impecável. 

- Voltar a praticar basquete. 

- Viajar pelo Brasil.

### Quanto ao teste ###

Primeiramente quero expor minha frustração por não ter conseguido completar o teste, mesmo achando praticamente impossível de terminar com a qualidade que eu busco para o meu código. Prefiri fazer pouco e bem feito do que muito e mal feito

Referente ao Redux, eu não coloquei em prática o pequeno conhecimento enferrujado que tenho, porque não quis empenhar tempo em algo que eu não conseguiria fazer bem feito em pouco tempo.

Quanto ao MockAPI, não consegui trazer um objeto com base no valor de uma prop sem o tal do %LIKE%. Quando rodo o endpoint https://60afd848e6d11e00174f5407.mockapi.io/user?username=ad, para verificar se existe o username "ad", a api me retorna o user com username "admin".

Sobre os teste unitários, eu não tenho experiência, portanto não empenhei meu tempo em aprender para a avaliação.

### Setup ###

1. Clona o repo (e monta o ambiente android api 29)

2. yarn

3. yarn go

4. Shazam

### Bônus ###

Me chama para bater um papo!! Gostaria muito de conversar para que me conheçam melhor, garanto que tenho muito à somar com o projeto!!

Mesmo não tendo completado o teste, estou muito confiante que vou gerar interesse pela Dotz.

Meu potencial é enorme, te garanto!

mateus.sousa@flowin.tech

(15) 98826 8074

https://www.linkedin.com/in/mateusbernardo/


